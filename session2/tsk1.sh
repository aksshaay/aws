'creating a new ebs volume'

aws ec2 create-volume --volume-type gp2 --size 2 --availability-zone ap-south-1


'descring ebs volumes:'
aws ec2 describe-volumes

'attaching ebs to instance'

aws ec2 attach-volume --volume-id vol-020f81b43733cc07f --instance-id i-08db6c5b5f3832dd9 --device /dev/sdf


'creating instance and attaching ebs in same command'

'creating a linux instance'
aws ec2 run-instances --image-id ami-09fb72da159a48a06 --count 1 --instance-type t2.micro --key-name a1 --security-group-ids sg-00ccb319c171864e3 --block-device-mappings "[{\"DeviceName\":\"/dev/sdf\",\"Ebs\":{\"VolumeSize\":20,\"DeleteOnTermination\":false}}]"


'Detaching a volume'
aws ec2 detach-volume --volume-id vol-020f81b43733cc07f

'creating a snapshot'
aws ec2 create-snapshot --volume-id vol-1234567890abcdef0 --description "This is my root volume snapshot"

'creating volumes from snapshots'

aws ec2 create-volume --volume-type gp2 --snapshot-id snap-01c3691923545fcaf --availability-zone ap-south-1a 



